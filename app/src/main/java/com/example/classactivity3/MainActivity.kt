package com.example.classactivity3

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Toast.makeText(applicationContext, "On Create()", Toast.LENGTH_SHORT).show();

    }

    override fun onRestart() {
        super.onRestart() //call to restart after onStop
        Toast.makeText(applicationContext, "On Restart()", Toast.LENGTH_SHORT).show();
    }

    override fun onStart() {
        super.onStart() //soon be visible
        Toast.makeText(applicationContext, "On Start()", Toast.LENGTH_SHORT).show();
    }

    override fun onResume() {
        super.onResume() //visible
        Toast.makeText(applicationContext, "On Resume()", Toast.LENGTH_SHORT).show();
    }

    override fun onPause() {
        super.onPause() //invisible
        Toast.makeText(applicationContext, "On Pause()", Toast.LENGTH_SHORT).show();
    }

    override fun onStop() {
        super.onStop()
        Toast.makeText(applicationContext, "On Stop()", Toast.LENGTH_SHORT).show();
    }

    override fun onDestroy() {
        super.onDestroy()
        Toast.makeText(applicationContext, "On Destroy()", Toast.LENGTH_SHORT).show();
    }
}